# 江demo



## `serve`服务端 (基于Koa2 + mysql)

----

### 命令

#### 安装依赖
```
npm install
```

#### 更新Sequlize表模型(每次更改数据库结构都要运行此命令)
```
npm run model
```

#### 开发环境下运行
```
npm run run dev
```

#### 生产环境下运行
```
npm start
```

#### Pm2下运行
```
npm run prd
```



### 配置

#### 配置文件
```
serve/
    |-bin 
    |-config/
    |------|-index.js
    |------|-dev.js   ## 开发环境配置
    |------|-prd.js   ## 生产环境配置
    |-src
    |-static
    |-app.js
```



#### 数据库 

```js
        db_config: {
            dialect: 'mysql',
            host: "127.0.0.1",
            database: 'jyj_v2_db',// 数据库名
            user: 'root',
            password: '',
            port: 3306
        }
```

#### 服务端口号

```js
    serve_config: {
        host: '127.0.0.1',
        port: 3001
    },
```



## `web`前端（客户端 + 后台管理)

----

### 安装依赖
```
npm install
```

### 运行
```
npm run serve
```

### 打包
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
