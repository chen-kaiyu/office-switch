import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import vuescrollConfig from '@/utils/vuescrollConfig.js'
import http from '@/utils/request'
import storage from '@/utils/storage'
import api from '@/utils/api'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import vuescroll from 'vuescroll' //  引入vuescroll
import 'vuescroll/dist/vuescroll.css' //  引入vuescroll样式
Vue.use(vuescroll) // 使用vuescroll
Vue.use(ElementUI);

import bus from '@/utils/bus.js'

const extend = Vue.prototype
extend.$http = http
extend.$bus = bus
extend.$storage = storage
extend.$api = api
extend.$base = process.env.VUE_APP_BASE
Vue.config.productionTip = false
Vue.prototype.$vuescrollConfig = vuescrollConfig

console.log('******* 环境 *******')
console.log('模式：', process.env.NODE_ENV)
console.log('API：', process.env.VUE_APP_URl)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')