import Vue from "vue"
import Router from 'vue-router';

Vue.use(Router)

const original = Router.prototype.push
Router.prototype.push = function push(location) {
    return original.call(this, location).catch(err => err)
}

const router = new Router({
    /* tip: 用history 这里遇到问题：放到服务器 空白 */
    // mode: 'history',
    routes: [
        {
            path: '/',
            name: 'client',
            meta: {
                title: '客户端'
            },
            component: () => import('@/views/Client/Index.vue'),
            children: [
                {
                    path: '/',
                    redirect: '/home'
                },
                {
                    name: 'home',
                    path: '/home',
                    meta: {
                        title: '首页'
                    },
                    component: () => import('@/views/Client/home/Index.vue')
                },
                {
                    name: 'login',
                    path: '/login',
                    meta: {
                        title: '登录'
                    },
                    component: () => import('@/views/Client/person/Login.vue')
                },
                {
                    name: 'register',
                    path: '/register',
                    meta: {
                        title: '注册'
                    },
                    component: () => import('@/views/Client/person/Register.vue')
                },
                {
                    name: 'work',
                    path: '/work',
                    meta: {
                        title: '文件详情'
                    },
                    component: () => import('@/views/Client/work/Index.vue')
                },
            ]
        },
        {
            name: 'adminLogin',
            path: '/admin/login',
            meta: {
                title: '管理员登录'
            },
            component: () => import('@/views/Admin/person/Login.vue')
        },
        {
            path: '/admin',
            name: 'admin',
            meta: {
                title: '后台管理'
            },
            component: () => import('@/views/Admin/Index.vue'),
            children: [
                {
                    path: '/admin',
                    name: 'adminHome',
                    meta: {
                        title: '后台管理--首页',
                    },
                    component: () => import('@/views/Admin/home/Index.vue')
                },
                {
                    path: '/admin/user',
                    name: 'adminUser',
                    meta: {
                        title: '后台管理--用户管理',
                    },
                    component: () => import('@/views/Admin/user/Index.vue')
                },
                {
                    path: '/admin/file',
                    name: 'adminFile',
                    meta: {
                        title: '后台管理--文件管理',
                    },
                    component: () => import('@/views/Admin/file/Index.vue')
                },
                {
                    path: '/admin/setting',
                    name: 'adminSetting',
                    meta: {
                        title: '后台管理--系统设置',
                    },
                    component: () => import('@/views/Admin/setting/Index.vue')
                },
            ]
        },
        {
            path: '*',
            name: 'NotFount',
            meta: {
                title: '页面不存在'
            },
            component: () => '@/assets/components/404'
        }]
})

router.beforeEach((to, from, next) => {
    document.title = to.meta.title
    if (to.meta.auth) { //权限判断
        if (localStorage.getItem('token')) { //读取token值
            //  成功
            next()
        } else {
            next({
                path: '/'
            })
        }
    } else {
        // 没有meta.auth 不用管
        next()
    }
});


export default router