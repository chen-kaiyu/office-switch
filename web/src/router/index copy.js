import Vue from "vue"
import Router from 'vue-router';
import Index from '@/pages/home/Index';
import PdfAdd from '@/pages/pdf/Add'
import PdfDetail from '@/pages/pdf/Detail'
import Login from '@/pages/login&register/Login'
import Register from '@/pages/login&register/Register'
import Work from '@/pages/work/Index'
import NotFount from '@/pages/commons/404'


Vue.use(Router)

const original = Router.prototype.push
Router.prototype.push = function push(location) {
    return original.call(this, location).catch(err => err)
}

const router = new Router({
    /* tip: 用history 这里遇到问题：放到服务器 空白 */
    // mode: 'history',
    routes: [{
        path: '/',
        name: 'Index',
        component: Index,
        meta: {
            token: true
        }
    }, {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    }, {
        path: '/work',
        name: 'Work',
        component: Work
    },
    {
        path: '/work2',
        name: 'Work2',
        component: () => import('@/pages/work/mock.vue')
    }, {
        path: '/pdf',
        name: 'Pdf',
        component: PdfDetail
    }, {
        path: '/pdf_add',
        name: 'PdfAdd',
        component: PdfAdd
    }, {
        path: '*',
        name: 'NotFount',
        component: NotFount
    }
    ]
})

router.beforeEach((to, from, next) => {
    if (to.meta.auth) { //权限判断
        if (localStorage.getItem('token')) { //读取token值
            //  成功
            next()
        } else {
            next({
                path: '/'
            })
        }
    } else {
        // 没有meta.auth 不用管
        next()
    }
});


export default router