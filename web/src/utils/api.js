export default {
    /* 文件 */
    API_FILE_GET:'/file/getAll',
    API_FILE_DETAIL:'/file/getDetail',
    API_FILE_DEL:'/file/del',
    API_FILE_COMMENT:'',
    API_FILE_EDIT: '/file/update',
    API_FILE_ADD: '/file/upload',

    /* 用户 */
    API_USER_LOGIN:'/user/login',
    API_USRE_REQUEST:'/user/request',
    API_USER_EDIT_INFO:'/user/updateInfo',
    API_USER_EDIT_PWD:'/user/updatePwd',

    /* 收藏夹 */
    
    
    /* 管理员 */
    API_ADMIN_SET_CAROUSEL:'/admin/editCarousel',
    API_ADMIN_SET_COLUMN:'/admin/editColumn',
}