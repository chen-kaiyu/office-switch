# 服务端 (基于Koa2 + mysql)



## 命令

### 安装依赖
```
npm install
```

### 更新Sequlize表模型(每次更改数据库结构都要运行此命令)
```
npm run model
```

### 开发环境下运行
```
npm run run dev
```

### 生产环境下运行
```
npm start
```

### Pm2下运行
```
npm run prd
```



## 配置 `文件路径 /config`

```
serve/
    |-bin 
    |-config/
    |------|-index.js
    |------|-dev.js   ## 开发环境配置
    |------|-prd.js   ## 生产环境配置
    |-src
    |-static
    |-app.js
```



### 数据库 

```js
        db_config: {
            dialect: 'mysql',
            host: "127.0.0.1",
            database: 'pptupload2',// 数据库名
            user: 'root',
            password: '00000OOOOO',
            port: 3306
        }
```

### 服务端口号

```js
    serve_config: {
        host: '127.0.0.1',
        port: 3001
    },
```

