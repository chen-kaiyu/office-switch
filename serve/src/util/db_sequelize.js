const Sequlize = require('sequelize')
const initModels = require('../model/init-models')
const { dialect, host, database, user, password } = require('../../config/index').db_config
const sequelize = new Sequlize(database, user, password, {
    host, dialect, dialectOptions: {
        dateStrings: true,
        typeCast: true
    }, define: { timestamps: true }, timezone: '+08:00' //改为标准时区
})

module.exports = initModels(sequelize)