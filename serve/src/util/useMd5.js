const crypto = require('crypto')

const useMd5 = (str) => {
    const md5 = crypto.createHash('md5')
    const result = md5.update(str).digest('hex') //hex表示拿到最终为十六进制
    return result
}

module.exports = useMd5
