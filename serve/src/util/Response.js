class Request {
    success({msg='Success',data=[]}){
        return {
            code:200,
            msg,
            data
        }
    }

    fail({ msg = 'Failed', data = [] }) {
        return {
            code: 403,
            msg,
            data
        }
    }

    error({ msg = 'Server error', data = [] }) {
        return {
            code: 500,
            msg,
            data
        }
    }
}

module.exports = new Request