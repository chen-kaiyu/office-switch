const toPdf = require("office-to-pdf");
const fs = require('fs');
const DB = require('./db_sequelize')

async function pptSwitch(item){
  let name='pdf'
  console.log('开始转换'); 
  //读取文件
  let wordBuffer = fs.readFileSync(item.destination + '/' + item.filename)
  //转换后文件存放地址
  let pptPostion=item.destination + '/' + 'ppt'+Date.now()+'.pdf'
  toPdf(wordBuffer).then(
    (pdfBuffer) => {
      //转换成pdf
      fs.writeFileSync(pptPostion, pdfBuffer)
    }, (err) => {
      console.log(err)
    }
  )
  let url=pptPostion.replace('static', '')
  let res = await DB.file.create({ name,  url  })
}

module.exports=pptSwitch