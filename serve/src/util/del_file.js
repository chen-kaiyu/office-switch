const fs = require('fs')

async function Delfile(url){
    if (fs.existsSync(url)) {
        await fs.unlinkSync(url)
        return true
    } else {
        throw `给定的路径${url}不存在，请给出正确的路径`
    }
}

module.exports = Delfile