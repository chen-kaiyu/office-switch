const Multer = require('koa-multer')
const MakeDir = require('./make_dir')
const CreateDate = require('./create_date')

let nowdate = CreateDate()
let fileDir;

let storage = Multer.diskStorage({
     destination(req, file, cb) {
        try{
            // 保存到目录
            fileDir = `static/upload/${file.fieldname}/${nowdate}`
            MakeDir(fileDir) // 生成日期目录
            cb(null, fileDir)
        }catch(e){
        }
    },
    // 文件重命名
    filename(req, file, cb) {
        let fileFormat = file.originalname.split('.')
        cb(null, Date.now() + '.' + fileFormat[fileFormat.length - 1])
    }
})

let uploadFile = Multer({
    storage: storage
})
module.exports = uploadFile