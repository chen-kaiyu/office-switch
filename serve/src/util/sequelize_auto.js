const SequlizeAuto = require('sequelize-auto')
const {dialect,host,database,user,password,port} = require('../../config/index').db_config

const options = {
    dialect,
    host,
    directory: './src/model',
    port,
    additional:{
        timestamps:false
    }
}

const auto = new SequlizeAuto(database,user,password,options)

auto.run(err => {
    if(err) throw err
})