function CreateDate() {
    let version = new Date(); //new 日期对象，获得一个实例

    //version = version.getDate();
    let year = version.getFullYear(); //获取年份
    let month = version.getMonth() + 1; //获取月份，默认月份是0-11
    let day = version.getDate(); //获取日期
    //下面是如果数值小于10的话，补0操作，看起来规整一点
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    version = year.toString() + '-' + month.toString() + '-' + day.toString();

    return version;

}

module.exports = CreateDate;