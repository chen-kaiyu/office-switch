//获取当前时间
let date = new Date();
let seperator1 = "-";
let seperator2 = ":";
let month = date.getMonth() + 1;
let minute = date.getMinutes();
let strDate = date.getDate();
if (month >= 1 && month <= 9) {
    month = "0" + month;
}
if (minute >= 1 && minute <= 9) {
    minute = "0" + minute;
}
if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
}
let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate +
    " " + date.getHours() + seperator2 + minute +
    seperator2 + date.getSeconds();

module.exports = currentdate
