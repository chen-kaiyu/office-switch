module.exports = (app) => {
    app.use(require('koa-bodyparser')({
        enableTypes: ['json', 'form', 'text']
    }))
    .use(require('koa-json')())
    .use(require('koa-bodyparser')())

    .use(require('koa-static')(__dirname + '/../../static'))

    // logger
    .use(async (ctx, next) => {
        const start = new Date()
        await next()
        const ms = new Date() - start
        console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
    })
    // 跨域
    .use(require('koa2-cors')())
}