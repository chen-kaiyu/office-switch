// controller
const UserController = require('../../controller/UserController')
const FileController = require('../../controller/FileController')
const CollectController = require('../../controller/CollectController')
const AdminController = require('../../controller/AdminController')
const UploadFile = require('../../util/upload_file')

const router = require('koa-router')()

    /* 用户 路由组 */
    .use('/user', require('koa-router')()
        .get('/', async (ctx) => { ctx.body = 'api/v1/user' })
        .post('/login', UserController.useLogin)
        .post('/register', UserController.useRegister)
        .post('/updatePwd', UserController.useUpdatePwd)
        .post('/updateInfo', UploadFile.single("avatar"), UserController.useUpdateInfo)

        .routes()
    )

    /* 收藏夹 路由组 */
    .use('/collect', require('koa-router')()
        .post('/getAll', CollectController.useGetAll)
        .post('/add', CollectController.useAdd)
        .post('/remove', CollectController.useRemove)

        .routes()
    )

    /* 文件 路由组 */
    .use('/file', require('koa-router')()
        .post('/upload', UploadFile.any(),FileController.uploadFile)
        .post('/del', FileController.useDelFile)
        .post('/update', UploadFile.single("file"), FileController.useUpdateFile)
        .get('/getAll', FileController.useGetAll)
        .get('/getDetail', FileController.getDetail)


        .routes()
    )

    /* 管理 路由组 */
    .use('/admin', require('koa-router')()
        .post('/addUser', AdminController.useAddUser)
        .post('/delUser', AdminController.useDelUser)// 注销用户只有超级管理员才具有的权限
        .post('/editUser', AdminController.useEditUser)
        .post('/editCarousel', AdminController.useEditCarousel)
        .post('/editColumn', AdminController.useEditColumn)

        .routes()
    )

module.exports = router