const DB = require('../util/db_sequelize')
const {
    Op
} = require('sequelize')
const Response = require('../util/Response') // 返回信息
const FormatTimeCurrent = require('../util/format_time') // 返回当前格式化时间
const useMd5 = require('../util/useMd5')

class CollectController {
    /**
     * 获取个人收藏
     */
    async useGetAll(ctx) {
        let { userId } = ctx.request.body
        const res = await DB.collect.findAll({ where: { userId } })
        if (res) {
            ctx.body = Response.success({ data:res })
        }
    }

    /**
     * 添加收藏
     */
    async useAdd(ctx) {
        let { userId, fileId } = ctx.request.body
        const res = await DB.collect.create({ userId, fileId, collectTime: FormatTimeCurrent })
        if (res.id) {
            ctx.body = Response.success({ msg: `添加收藏成功` })
        }
    }


    /**
     * 添加收藏
     */
    async useAdd(ctx) {
        let { userId, fileId } = ctx.request.body
        const res = await DB.collect.create({ userId, fileId, collectTime: FormatTimeCurrent })
        if (res.id) {
            ctx.body = Response.success({ msg: `添加收藏成功` })
        }
    }

    /**
     * 移除收藏
     */
    async useRemove(ctx) {
        let { userId, fileId } = ctx.req.body
        const res = await DB.collect.destroy({ where: { userId, fileId } })
        if (res) {
            ctx.body = Response.success({ msg: `移除收藏成功` })
        }
    }
}

module.exports = new CollectController