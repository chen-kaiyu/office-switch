const DB = require('../util/db_sequelize')
const {
    Op
} = require('sequelize')
const Response = require('../util/Response') // 返回信息
const FormatTimeCurrent = require('../util/format_time') // 返回当前格式化时间

class AdminController {
    /**
     * 新增员工
     */
    async useAddUser(ctx){

    }

    /**
     * 删除员工
     */
    async useDelUser(ctx) {

    }

    /**
     * 删除员工
     */
    async useEditUser(ctx) {

    }

    /**
     * 编辑首页轮播图
     */
    async useEditCarousel(ctx) {

    }

    /**
     * 编辑首页栏目
     */
    async useEditColumn(ctx) {

    }
}

module.exports = new AdminController