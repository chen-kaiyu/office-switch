const DB = require('../util/db_sequelize')
const {
    Op
} = require('sequelize')
const useMd5 = require('../util/useMd5')
const Response = require('../util/Response') // 返回信息
const FormatTimeCurrent = require('../util/format_time') // 返回当前格式化时间

class UserController {
    /**
     * 用户注册
     */
    async useRegister(ctx) {
        // 获取
        let { account, password, nickname, role = 0 } = ctx.request.body

        // 查询是否存在同一用户
        const sameAccount = await DB.user.findOne({ where: { account } })
        if (sameAccount === null) {
            const res = await DB.user.create({
                account,
                password: useMd5(password),
                nickname,
                role,
                regTime: FormatTimeCurrent
            })
            ctx.body = Response.success({})
        } else {
            ctx.body = Response.fail({ msg: '抱歉，该用户已注册过' })
        }
    }


    /**
     * 用户登录
     */
    async useLogin(ctx) {
        // 获取
        let { account, password } = ctx.request.body

        // 查询账户密码 同时设置登录时间
        const res = await DB.user.update({ lastTime: FormatTimeCurrent }, { where: { account, password: useMd5(password) } })

        if (res[0] != 0) {
            ctx.body = Response.success({ msg: '登录成功'})
        } else {
            ctx.body = Response.fail({ msg: '抱歉，账户或密码错误' })
        }
    }

    /**
     * 用户更新账户密码
     */
    async useUpdatePwd(ctx) {
        // 获取
        let { account, oldPwd, newPwd } = ctx.request.body

        // 更新
        let res = await DB.user.update({ password: useMd5(newPwd) }, { where: { account,password: useMd5(oldPwd)} }) // res : []

        if (res[0] != 0) {
            ctx.body = Response.success({msg:'密码重置成功,请重新登录'})
        } else {
            ctx.body = Response.fail({ msg: '抱歉，密码重置失败，请检查原始密码' })
        }
    }

    /**
     * 用户更新个人信息
     */
    async useUpdateInfo(ctx) {
        if (!ctx.req.file) return // 文件上传失败
        // 获取
        let { account, nickname, avatar } = ctx.req.body

        // 获取头像url
        avatar = avatar || (ctx.req.file.destination + '/' + ctx.req.file.filename).replace('static', '')

        // 更新
        let res = await DB.user.update({ nickname, avatar }, { where: { account } }) // res : []

        if (res[0] != 0) {
            ctx.body = Response.success({ msg: '更新信息成功' })
        } else {
            ctx.body = Response.fail({ msg: '更新信息失败' })
        }
    }



}

module.exports = new UserController