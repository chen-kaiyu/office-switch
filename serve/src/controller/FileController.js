const DB = require('../util/db_sequelize')
const {
    Op
} = require('sequelize')
const DelFile = require('../util/del_file')
const Response = require('../util/Response') // 返回信息
const FormatTimeCurrent = require('../util/format_time') // 返回当前格式化时间
const pptSwitch = require('../util/ppt_pdf')
const path=require('path')
const { rejects } = require('assert')
let releaseTime = FormatTimeCurrent
class FileController {
    /**
     * 上传文件
     * 
     */
    async uploadFile(ctx) {
        // if (!ctx.req.file) return // 文件上传失败
        let { name, userId, describe, type } = ctx.req.body
        // 声明封面、文件路径
        let url, coverPic
        const files = ctx.req.files
        files.forEach((item) => {
            if (item.fieldname == 'file') {
                // 文件路径
                url = (item.destination + '/' + item.filename).replace('static', '')
                // if(path.extname(item.filename)=='.ppt' || path.extname(item.filename)=='.pptx'){
                //     console.log(item);
                pptSwitch(item)
                    // console.log(url);
                // } else {
                    
                // }
            } else if (item.fieldname == 'image') {
                // 封面路径
                coverPic = (item.destination + '/' + item.filename).replace('static', '')
            } else {
                // 非法上传文件则删除
                DelFile(item.path)
            }
        })      

        // 发布时间
            try {
                let res = await DB.file.create({ name, userId, describe, url, coverPic, type, releaseTime })
                ctx.body = Response.success({})
            } catch (e) {
                files.forEach((item) => {
                    DelFile(item.path)+2    
                })
                // throw e
                ctx.body = Response.error({ data: e })
            }
        }


    /**
     * 删除文件
     */
    async useDelFile(ctx) {
        let { id } = ctx.request.body
        let { url, coverPic } = await DB.file.findOne({ where: { id } })
        DelFile(`static${url}`)
        if (coverPic) {
            DelFile(`static${coverPic}`)
        }

        let res = await DB.file.destroy({ where: { id } })
        if (res) {
            ctx.body = Response.success({ msg: '删除成功' })
        }
    }

    /**
     * 更新文件
     */
    async useUpdateFile(ctx) {
        ctx.body = Response.fail({ msg: 'api未完成' })
    }

    /**
     * 首页获取文件
     */
    async useGetAll(ctx) {
        let { size = 5, page = 1 } = ctx.query
        size = Number(size)
        page = Number(page)
        let res = await DB.file.findAll({ offset: (page - 1) * size, limit: size, order: [['id', 'DESC']] })

        // 获取条数
        let count = await DB.file.count()
        let data = {
            page,
            size,
            count,
            res
        }
        ctx.body = Response.success({ data })
    }

    /**
     * 获取指定文件详情
     */
    async getDetail(ctx) {
        let { id } = ctx.query
        let res = await DB.file.findOne({ where: { id } })
        ctx.body = Response.success({ data: res })
    }

}

module.exports = new FileController