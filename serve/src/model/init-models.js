var DataTypes = require("sequelize").DataTypes;
var _collect = require("./collect");
var _file = require("./file");
var _user = require("./user");

function initModels(sequelize) {
  var collect = _collect(sequelize, DataTypes);
  var file = _file(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes);


  return {
    collect,
    file,
    user,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
