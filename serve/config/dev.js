/**
 * 开发环境配置
 */

module.exports = {
    serve_config: {
        host: '127.0.0.1',
        port: 3001
    },
    jwt_config: {
        serect: 'loNwN9kkR',
        expiresIn: '1h' // 60 * 60，"2 days"，"10h"，"7d" 
    },
    db_config: {
        dialect: 'mysql',
        host: "127.0.0.1",
        database: 'pptupload2',
        user: 'root',
        password: '00000OOOOO',
        port: 3306
    }
}