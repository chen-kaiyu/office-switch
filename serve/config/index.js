const dev = require('./dev')
const prd = require('./prd')

//根据不同的NODE_ENV，输出不同的配置对象，默认输出dev的配置对象
module.exports = {
    dev,
    prd
} [process.env.NODE_ENV || 'dev']