const Koa = require('koa')
const app = new Koa()
const onerror = require('koa-onerror')
const middleware = require('./src/middleware')
const api = require('koa-router')()

const v1 = require('./src/api/v1')

// error handler
onerror(app)

// middlewares
middleware(app)

// routes
api.use('/api/v1', v1.routes())
app.use(api.routes(), api.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});

module.exports = app
